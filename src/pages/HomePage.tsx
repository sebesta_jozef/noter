import React, { FunctionComponent, useEffect } from 'react';
import {connect} from 'react-redux'
import {isEmpty} from 'lodash'
import {compose} from 'recompose'
import {translate} from 'react-switch-lang'
import CircularProgress from '@material-ui/core/CircularProgress'
import {Snackbar, Button} from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'

import {notesListRequest, resetFlags, createNoteModalOpen} from '../models/notes/actions'
import BasicLayout from '../components/BasicLayout'
import Note from '../components/Note'
import CreateNoteModal from '../components/CreateNoteModal'

import { NoteDataModel } from "../interfaces/NoteModel";
import { RootState } from "../rootReducer";
import AddIcon from "@material-ui/icons/Add";
import UpdateNoteModal from "../components/UpdateNoteModal";
import DetailNoteModal from "../components/DetailNoteModal";
import DeleteNoteModal from "../components/DeleteNoteModal";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>

    createStyles({
          homepage: {
            padding: "12px"
          },
          buttonSection: {
            marginTop: "30px"
          },
          lowerSection: {
            marginTop: "30px"
          },
          snackbar: {
            marginTop: "70px"
          }
        }
    )
)


interface Props {
  notesListRequest: (() => void);
  createNoteModalOpen: ((arg0?:boolean) => void);
  resetFlags: (() => void);
  isDeleted: boolean;
  isCreated: boolean;
  isUpdated: boolean;
  t: ((arg0: string) => string),
  notesData: NoteDataModel[],
  loading: boolean
}


const _Homepage: FunctionComponent<Props> = props => {
  const {t, isUpdated, isCreated, isDeleted, notesData, resetFlags, loading, notesListRequest, createNoteModalOpen} = props

  const classes = useStyles();

  useEffect(() => {
    notesListRequest()
  }, [notesListRequest])

  const closeSnackbar = () => {
    resetFlags()
  }

  const getSnackbarText = () => {
    let txtVar
    if (!isDeleted && !isUpdated && !isCreated) {
      return ''
    }
    if (isCreated) {
      txtVar = 'Created'
    } else if (isDeleted) {
      txtVar = 'Deleted'
    } else if (isUpdated) {
      txtVar = 'Updated'
    }
    return t(`Snackbar.note${txtVar}`)
  }

  const snackbarText = getSnackbarText()

  return (
    <BasicLayout>
      <div className={classes.homepage}>
        <div>
          <Snackbar
            open={
              isUpdated ||
              isCreated ||
              isDeleted
            }
            autoHideDuration={3000}
            onClose={closeSnackbar}
            anchorOrigin={{vertical: 'top', horizontal: 'center'}}
            className={classes.snackbar}
          >
            <Alert onClose={closeSnackbar} severity="success">
              {snackbarText}
            </Alert>
          </Snackbar>
          <CreateNoteModal />
          <div className={classes.buttonSection}>
            <Button
                onClick={() => createNoteModalOpen(true)}
                variant="contained"
                color="secondary"
            >
              <AddIcon/>
              {t('Button.add')}
            </Button>
          </div>
        </div>
        <div className={classes.lowerSection}>
          {!!loading ? (
            <CircularProgress />
          ) : !isEmpty(notesData) ? (
            <React.Fragment>
              <DetailNoteModal />
              <UpdateNoteModal />
              <DeleteNoteModal />
              {notesData.map(note => (
                <Note key={note.id} noteData={note} />
              ))}
            </React.Fragment>
          ) : (
            <div>{t('Homepage.noNotes')}</div>
          )}
        </div>
      </div>
    </BasicLayout>
  )

}

const Homepage = compose(
  connect(
      (reduxState: RootState) => ({
      notesData: reduxState.notes.notesData,
      loading: reduxState.loadings.NOTES_LIST,
      isUpdated: reduxState.notes.isUpdated,
      isDeleted: reduxState.notes.isDeleted,
      isCreated: reduxState.notes.isCreated,
    }),
    {notesListRequest, resetFlags, createNoteModalOpen}
  ),
  translate
)(_Homepage)

export default Homepage
