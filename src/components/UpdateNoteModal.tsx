import React, { FunctionComponent} from 'react';
import {DialogTitle, Dialog} from '@material-ui/core'
import {connect} from 'react-redux'
import {compose} from 'recompose'
import {translate} from 'react-switch-lang'

import {updateNoteModalOpen} from '../models/notes/actions'

import { RootState } from "../rootReducer";
import UpdateNoteForm from "./UpdateNoteForm";


interface Props {
    t: ((arg0: string) => string);
    isUpdateModalOpened: boolean,
    updateNoteModalOpen: ((arg0:boolean) => void),
}


const _UpdateNoteModal: FunctionComponent<Props> = props => {
    const {t, isUpdateModalOpened, updateNoteModalOpen} = props

    return (
        <React.Fragment>
            { isUpdateModalOpened && (
                <Dialog
                    onClose={() => updateNoteModalOpen(false)}
                    open
                    fullWidth
                    maxWidth="md"
                >
                    <DialogTitle>
                        {t('Form.editNote')}
                    </DialogTitle>
                    <UpdateNoteForm onClose={() => updateNoteModalOpen(false)} />
                </Dialog>
            )}
        </React.Fragment>
    )
}

const UpdateNoteModal = compose(
    connect(
        (reduxState: RootState) => ({
            isUpdateModalOpened: reduxState.notes.isUpdateModalOpened
        }),
        {updateNoteModalOpen}
    ),
    translate
)(_UpdateNoteModal)

export default UpdateNoteModal
