import {Route, Redirect} from "react-router-dom";
import auth from "../lib/auth";
import React from "react";

const PrivateRoute = ({component: Component, ...rest}) => (
    <Route {...rest}
           render={props =>
               auth.isAuthenticated() ? (
                   <Component {...props} />
               ) : (
                    <Redirect to={{pathname: "/login"}}/>
               )
           }
    />
)

export default PrivateRoute;