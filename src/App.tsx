import React from 'react'
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom'
import {Provider} from 'react-redux'

import {sagaMiddleware, store} from './store'
import rootSaga from './rootSaga'
import HomePage from './pages/HomePage'


import {
  setTranslations,
  setDefaultLanguage,
  setLanguageCookie,
} from 'react-switch-lang'

import en from './translations/en.json'
import cz from './translations/cz.json'
import Cookies from 'js-cookie'
import LoginPage from "./pages/LoginPage";
import AnonymousRoute from "./components/AnonymousRoute";
import PrivateRoute from "./components/PrivateRoute";
import ErrorPage from "./pages/ErrorPage";

sagaMiddleware.run(rootSaga)

setTranslations({en, cz})
setDefaultLanguage(Cookies.get('language') || 'en')

setLanguageCookie()

const _App: React.FunctionComponent = () => {
    return (
      <Provider store={store}>
        <Router>
              <Switch>
                  <PrivateRoute exact path="/" component={HomePage} />
                  <AnonymousRoute exact path="/login" component={LoginPage} />
                  <Route path={"/error-not-found"} exact component={props => <ErrorPage {...props} errorCode={404} errorMessage={"We cant find page you are looking for"} />} />
                  <Route path={"/error-service-unavailable"} exact component={props => <ErrorPage {...props} errorCode={502} errorMessage={"Service unavailable"} />} />
                  <Redirect from={"*"} to={"/error-not-found"} />
              </Switch>
          </Router>
      </Provider>
    )
}

export default _App
