import {Route, Redirect} from "react-router-dom";
import auth from "../lib/auth";
import React from "react";

const AnonymousRoute = ({component: Component, ...rest}) => (
    <Route {...rest}
        render={props =>
            auth.isAuthenticated() ? (
                <Redirect to={{pathname: "/"}}/>
            ) : (
                <Component {...props} />
            )
        }
    />
)

export default AnonymousRoute;