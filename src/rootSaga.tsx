import {all} from 'redux-saga/effects'
import {
  watchNotesListRequest,
  watchNoteDetailRequest,
  watchNoteUpdateRequest,
  watchNoteDeleteRequest,
  watchNoteCreateRequest,
} from './models/notes/sagas'

import {
  watchLoginRequest,
  watchHealthCheckRequest
} from './models/authentication/sagas'

export default function* rootSaga() {
  yield all([
    watchNotesListRequest(),
    watchNoteDetailRequest(),
    watchNoteUpdateRequest(),
    watchNoteDeleteRequest(),
    watchNoteCreateRequest(),
    watchLoginRequest(),
    watchHealthCheckRequest()
  ])
}
