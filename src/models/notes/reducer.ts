import {
  CREATE_NOTE_MODAL_OPEN,
  DELETE_NOTE_MODAL_OPEN,
  DETAIL_NOTE_MODAL_OPEN,
  UPDATE_NOTE_MODAL_OPEN,
  NOTE_CREATE_SUCCESS,
  NOTE_DELETE_SUCCESS,
  NOTE_DETAIL_SUCCESS,
  NOTE_UPDATE_SUCCESS,
  NOTES_LIST_SUCCESS,
  RESET_FLAGS,
} from './constants'

import {NoteDataModel} from './../../interfaces/NoteModel'

import {NoteActionTypes} from "./actions.interfaces";


export type State = {
  readonly isUpdated: boolean;
  readonly isCreated: boolean;
  readonly isDeleted: boolean;
  readonly isCreateModalOpened: boolean;
  readonly isUpdateModalOpened: boolean;
  readonly isDetailModalOpened: boolean;
  readonly isDeleteModalOpened: boolean;
  readonly noteData: NoteDataModel;
  readonly notesData: NoteDataModel[];
  readonly noteId: string;
  readonly noteTitle: string;
};


const initialState: State = {
  notesData: [],
  noteData: {id: "", title: "", description: "", modifiedAt: ""},
  isUpdated: false,
  isDeleted: false,
  isCreated: false,
  isCreateModalOpened: false,
  isUpdateModalOpened: false,
  isDetailModalOpened: false,
  isDeleteModalOpened: false,
  noteId: "",
  noteTitle: ""
}


const reducer = (state = initialState, action: NoteActionTypes) => {
  switch (action.type) {
    case NOTES_LIST_SUCCESS:
      return {
        ...state,
        notesData: action.notesData,
      }

    case NOTE_DETAIL_SUCCESS:
      return {
        ...state,
        noteData: action.noteData,
      }
    case NOTE_UPDATE_SUCCESS:
      return {
        ...state,
        isUpdated: action.isUpdated,
        isUpdateModalOpened: false,
      }
    case NOTE_DELETE_SUCCESS:
      return {
        ...state,
        isDeleted: action.isDeleted,
        isDeleteModalOpened: false,
      }
    case NOTE_CREATE_SUCCESS:
      return {
        ...state,
        isCreated: action.isCreated,
      }
    case CREATE_NOTE_MODAL_OPEN:
      return {
        ...state,
        isCreateModalOpened: action.isOpened,
      }
    case UPDATE_NOTE_MODAL_OPEN:
      return {
        ...state,
        isUpdateModalOpened: action.isOpened,
        noteData: action.noteData
      }
    case DETAIL_NOTE_MODAL_OPEN:
      return {
        ...state,
        isDetailModalOpened: action.isOpened,
        noteData: action.noteData
      }
    case DELETE_NOTE_MODAL_OPEN:
      return {
        ...state,
        isDeleteModalOpened: action.isOpened,
        noteId: action.noteId,
        noteTitle: action.noteTitle
      }
    case RESET_FLAGS:
      return {
        ...state,
        isUpdated: action.isUpdated,
        isDeleted: action.isDeleted,
        isCreated: action.isCreated,
      }
    default:
      return state
  }
}

export default reducer
