import {combineReducers} from 'redux'

import loadingSharedReducer from './models/loadingShared/reducer'
import notesReducer from './models/notes/reducer'
import authenticationReducer from './models/authentication/reducer'

export const rootReducer = combineReducers({
  notes: notesReducer,
  loadings: loadingSharedReducer,
  authentication: authenticationReducer,
})

export type RootState = ReturnType<typeof rootReducer>
