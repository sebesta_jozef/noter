# Noter FE - react app

### Installing

```
npm install
```

## Deployment

```
npm start
```

### React app runs by default on port 9000

### You need to have backend running !

## Jar version of noter BE including migrated sqlite DB file is located here:

https://bitbucket.org/sebesta_jozef/noter-be-jar/src/master/

### REST Backend service runs by default on port 8080 

## You can log into the app with these credentials:

````
username:admin

password:admin
````




## Source code of noter BE is located here:

https://bitbucket.org/sebesta_jozef/noter-be/src/master/