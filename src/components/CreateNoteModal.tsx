import React, { FunctionComponent} from 'react';
import {DialogTitle, Dialog} from '@material-ui/core'
import {connect} from 'react-redux'
import {compose} from 'recompose'
import {translate} from 'react-switch-lang'

import {noteCreateRequest, createNoteModalOpen} from '../models/notes/actions'

import { RootState } from "../rootReducer";
import CreateNoteForm from "./CreateNoteForm";


interface Props {
  t: ((arg0: string) => string);
  isCreateModalOpened: boolean,
  createNoteModalOpen: ((arg0:boolean) => void),
}


const _CreateNoteModal: FunctionComponent<Props> = props => {
    const {t, isCreateModalOpened, createNoteModalOpen} = props

    return (
        <React.Fragment>
            {isCreateModalOpened && (
                <Dialog
                    onClose={() => createNoteModalOpen(false)}
                    open
                    fullWidth
                    maxWidth="md"
                >
                    <DialogTitle>
                        {t('Form.createNote')}
                    </DialogTitle>
                    <CreateNoteForm
                        onClose={() => createNoteModalOpen(false)}
                    />
                </Dialog>
            )}
        </React.Fragment>
    )
}

const CreateNoteModal = compose(
  connect(
      (reduxState: RootState) => ({
      loading: reduxState.loadings.NOTE_CREATE,
      isCreateModalOpened: reduxState.notes.isCreateModalOpened
    }),
    {noteCreateRequest, createNoteModalOpen}
  ),
  translate
)(_CreateNoteModal)

export default CreateNoteModal
