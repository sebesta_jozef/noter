import React, { FunctionComponent} from 'react';
import {DialogTitle, Dialog, DialogContent, DialogActions, Button, CircularProgress} from '@material-ui/core'
import {connect} from 'react-redux'
import {compose} from 'recompose'
import {translate} from 'react-switch-lang'

import {deleteNoteModalOpen, noteDeleteRequest} from '../models/notes/actions'

import { RootState } from "../rootReducer";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        dialogActions: {
            marginTop: theme.spacing(3),
            display: "flex",
            justifyContent: "left",
            paddingLeft: "24px"
        },
    })
);


interface Props {
    t: ((arg0: string) => string);
    isDeleteModalOpened: boolean,
    noteId: string,
    noteTitle: string,
    deleteNoteModalOpen: ((arg0:boolean) => void),
    noteDeleteRequest: ((arg0: string) => void);
    loading: boolean,
}


const _DeleteNoteModal: FunctionComponent<Props> = props => {
    const {t, isDeleteModalOpened, deleteNoteModalOpen, noteId, noteDeleteRequest, loading, noteTitle} = props
    const classes = useStyles();
    return (
        <React.Fragment>
            { isDeleteModalOpened && (
                <Dialog
                    onClose={() => deleteNoteModalOpen(false)}
                    open
                    fullWidth
                    maxWidth="sm"
                >
                    <DialogTitle>
                        {t('Form.deleteNote')}
                    </DialogTitle>
                    <DialogContent>
                        {`${t('Modal.deleteConfirmText')} "${noteTitle}" ?`}
                    </DialogContent>
                    <DialogActions className={classes.dialogActions}>
                        <Button
                            onClick={() => noteDeleteRequest(noteId)}
                            variant="contained"
                            color="secondary"
                            type="submit"
                            size="large"
                            disabled={!!loading}
                        >
                            {loading && <CircularProgress size={14} />}
                            {!loading && t('Button.delete')}
                        </Button>
                        <Button
                            autoFocus
                            onClick={() => deleteNoteModalOpen(false)}
                            variant={'outlined'}
                            color="primary"
                            size="large"
                        >
                            {t('Button.cancel')}
                        </Button>
                    </DialogActions>
                </Dialog>
            )}
        </React.Fragment>
    )
}

const DeleteNoteModal = compose(
    connect(
        (reduxState: RootState) => ({
            loading: reduxState.loadings.NOTE_DELETE,
            isDeleteModalOpened: reduxState.notes.isDeleteModalOpened,
            noteId: reduxState.notes.noteId,
            noteTitle: reduxState.notes.noteTitle,
        }),
        {deleteNoteModalOpen, noteDeleteRequest}
    ),
    translate
)(_DeleteNoteModal)

export default DeleteNoteModal
