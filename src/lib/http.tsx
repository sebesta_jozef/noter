import axios from "axios/index";
import auth from "./auth";

const http = axios.create()

const jwtToken = auth.token()

http.interceptors.request.use(
    function(config) {
        if (jwtToken) {
            config.headers["authorization"] = "Bearer " + jwtToken;
        }
        return config;
    },
    function(err) {
        return Promise.reject(err);
    }
);

http.interceptors.response.use(
    response => response,
    function (error) {
        if (error.message === 'Network Error') {
            window.location.href = "/error-service-unavailable"
        }
        if (error.response && error.response.status === 401) {
            auth.deauthenticate()
        }

        return Promise.reject(error)
    }
)

export default http;