import React, { FunctionComponent} from 'react';
import {Form, Formik} from 'formik'
import {connect} from 'react-redux'
import {compose} from 'recompose'
import {translate} from 'react-switch-lang'

import {noteCreateRequest} from '../models/notes/actions'

import { RootState } from "../rootReducer";
import * as yup from "yup";
import NoteFormFields from "./NoteFormFields";


interface Props {
    t: ((arg0: string) => string);
    noteCreateRequest: ((arg0: string, arg1: string) => void),
    loading: boolean,
    onClose: (() => void),
}


const _CreateNoteForm: FunctionComponent<Props> = props => {
    const {t, loading, noteCreateRequest, onClose} = props

    const validationSchema = yup.object().shape({
        title: yup.string().required(t('Form.required')),
        description: yup.string().required(t('Form.required')),
    });

    return (
        <React.Fragment>
            <Formik
                initialValues={{title: "", description: ""}}
                validationSchema={validationSchema}
                onSubmit={(values, {resetForm}) => {
                    noteCreateRequest(values.title, values.description)
                    resetForm({})
                }}
            >
                {({handleSubmit}) => (
                    <Form onSubmit={handleSubmit}>
                        <NoteFormFields loading={loading} onClose={onClose} />
                    </Form>
                )}
            </Formik>
        </React.Fragment>
    )
}


const CreateNoteForm = compose(
    connect(
        (reduxState: RootState) => ({
            loading: reduxState.loadings.NOTE_CREATE,
        }),
        {noteCreateRequest}
    ),
    translate
)(_CreateNoteForm)

export default CreateNoteForm