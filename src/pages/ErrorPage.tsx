import React, {useEffect} from 'react'
import {FunctionComponent} from "react";
import {Card, CardContent, CardHeader, Container, CssBaseline} from "@material-ui/core";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {connect} from "react-redux";
import {compose} from 'recompose';
import {RootState} from "../rootReducer";
import {healthCheckRequest} from "../models/authentication/actions";
import {useHistory} from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>

    createStyles({
        errorMessage: {
            display: "flex",
            justifyContent:"center"}
        }
    )
)

interface Props {
    errorCode: number
    errorMessage:string,
    isServerUp:boolean,
    healthCheckRequest: (() => void);
}

const _ErrorPage: FunctionComponent<Props> = props => {
    const {errorCode, errorMessage, healthCheckRequest, isServerUp} = props;
    const classes = useStyles();
    const history = useHistory()

    useEffect(() => {
        setInterval(() => {
            healthCheckRequest()
        }, 5000)
    })

    if (errorCode === 502 && isServerUp) {
        history.push("/login")
    }

    return (
        <React.Fragment>
            <CssBaseline />
            <Container maxWidth="sm">
                <Card>
                    <CardHeader style={{textAlign: 'center'}} title={`Error: ${errorCode}`} />
                    <CardContent>
                        <div className={classes.errorMessage}>
                            {errorMessage}
                        </div>
                    </CardContent>
                </Card>
            </Container>
        </React.Fragment>
    )
}


const ErrorPage = compose(
    connect(
        (reduxState: RootState) => ({
            isServerUp: reduxState.authentication.isServerUp,
        }),
        {healthCheckRequest}
    ),
)(_ErrorPage);

export default ErrorPage