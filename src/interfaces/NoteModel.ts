export interface NoteDataModel {
    id: string;
    title: string;
    description: string;
    modifiedAt: string;
}
