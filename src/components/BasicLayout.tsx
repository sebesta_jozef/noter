import React, { FunctionComponent} from 'react';
import CssBaseline from '@material-ui/core/CssBaseline'
import Typography from '@material-ui/core/Typography'
import blue from '@material-ui/core/colors/blue';
import lightBlue from '@material-ui/core/colors/lightBlue';
import {createMuiTheme, MuiThemeProvider} from "@material-ui/core/styles";
import {setLanguage, translate} from 'react-switch-lang'
import {AppBar, Box, Button, Toolbar, Tooltip} from "@material-ui/core";
import ExitToApp from '@material-ui/icons/ExitToApp'
import auth from "../lib/auth";
import {useHistory} from "react-router-dom";


interface Props {
    children: React.ReactNode,
    t: ((arg0: string) => string),
}

const theme = createMuiTheme({
    palette: {
        primary: {
            main: blue[500],
        },
        secondary: {
            main: lightBlue[500],
        },
    },
});

const handleSetLanguage = (key:string) => () => {
    setLanguage(key)
}


const BasicLayout:FunctionComponent<Props> = props => {
    const history = useHistory();
    const {t} = props;

    const handleLogout = ():void => {
        auth.deauthenticate()
        history.push("/login")
    }

    return (
      <MuiThemeProvider theme={theme}>
        <React.Fragment>
            <CssBaseline/>
            <AppBar position="relative">
                <Toolbar>
                    <Box display='flex' flexGrow={1}>
                        <Typography variant="h6" color="inherit" noWrap>
                            {t('App.title')}
                        </Typography>
                    </Box>
                    <Box>
                        {
                            auth.isAuthenticated() && (
                                <Tooltip title={t('App.logout')}>
                                    <Button type="button" onClick={handleLogout}>
                                        <ExitToApp/>
                                    </Button>
                                </Tooltip>
                            )
                        }
                        <Button type="button" onClick={handleSetLanguage('en')}>
                            EN
                        </Button>
                        <Button type="button" onClick={handleSetLanguage('cz')}>
                            CZ
                        </Button>
                    </Box>
                </Toolbar>
            </AppBar>
            <main>
                {props.children}
            </main>
        </React.Fragment>
      </MuiThemeProvider>
    )
}

export default translate(BasicLayout);
