import React, { FunctionComponent} from 'react';
import {Form, Formik} from 'formik'
import {connect} from 'react-redux'
import {compose} from 'recompose'
import {translate} from 'react-switch-lang'

import {noteUpdateRequest} from '../models/notes/actions'

import { RootState } from "../rootReducer";
import * as yup from "yup";
import {NoteDataModel} from "../interfaces/NoteModel";
import NoteFormFields from "./NoteFormFields";


interface Props {
    t: ((arg0: string) => string);
    noteUpdateRequest: ((arg0: NoteDataModel) => void),
    loading: boolean,
    noteData: NoteDataModel,
    onClose: (() => void),
}


const _UpdateNoteForm: FunctionComponent<Props> = props => {
    const {t, loading, noteUpdateRequest, noteData, onClose} = props

    const validationSchema = yup.object().shape({
        title: yup.string().required(t('Form.required')),
        description: yup.string().required(t('Form.required')),
    });

    return (
        <React.Fragment>
            <Formik
                initialValues={{title:noteData.title, description:noteData.description}}
                validationSchema={validationSchema}
                onSubmit={(values) => {
                    const updatedNoteData = Object.assign(noteData, {title: values.title, description: values.description})
                    noteUpdateRequest(updatedNoteData)
                }}
            >
                {({handleSubmit}) => (
                    <Form onSubmit={handleSubmit}>
                        <NoteFormFields loading={loading} onClose={onClose} />
                    </Form>
                )}
            </Formik>
        </React.Fragment>
    )
}


const UpdateNoteForm = compose(
    connect(
        (reduxState: RootState) => ({
            loading: reduxState.loadings.NOTE_UPDATE,
            noteData: reduxState.notes.noteData,
        }),
        {noteUpdateRequest}
    ),
    translate
)(_UpdateNoteForm)

export default UpdateNoteForm