import React, {FunctionComponent} from 'react';
import {DialogTitle, Dialog, DialogContent, CircularProgress, Button, DialogActions} from '@material-ui/core'
import {connect} from 'react-redux'
import {compose} from 'recompose'
import {translate} from 'react-switch-lang'

import {detailNoteModalOpen} from '../models/notes/actions'

import { RootState } from "../rootReducer";
import {NoteDataModel} from "../interfaces/NoteModel";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        dialogActions: {
            marginTop: theme.spacing(3),
            display: "flex",
            justifyContent: "left",
            paddingLeft: "24px"
        },
        description : {
            marginTop: "30px"
        }
    })
);


interface Props {
    t: ((arg0: string) => string);
    isDetailModalOpened: boolean,
    detailNoteModalOpen: ((arg0:boolean) => void),
    noteData: NoteDataModel,
    loading: boolean,
}


const _DetailNoteModal: FunctionComponent<Props> = props => {
    const {t, isDetailModalOpened, detailNoteModalOpen, noteData, loading} = props
    const classes = useStyles();

    return (
        <React.Fragment>
            { isDetailModalOpened && (
                <Dialog
                    onClose={() => detailNoteModalOpen(false)}
                    open
                    fullWidth
                    maxWidth="md"
                >
                    <DialogTitle>
                        {t('Form.detailNote')}
                    </DialogTitle>
                    <DialogContent>
                        {!!loading ? (
                            <CircularProgress />
                        ) : (
                            <React.Fragment>
                                <p>{noteData.title}</p>
                                <p className={classes.description}>{noteData.description}</p>
                            </React.Fragment>
                        )}
                    </DialogContent>
                    <DialogActions className={classes.dialogActions}>
                        <Button
                            autoFocus
                            onClick={() => detailNoteModalOpen(false)}
                            variant={'outlined'}
                            color="primary"
                            size="large"
                        >
                            {t('Button.cancel')}
                        </Button>
                    </DialogActions>
                </Dialog>
            )}
        </React.Fragment>
    )
}

const DetailNoteModal = compose(
    connect(
        (reduxState: RootState) => ({
            loading: reduxState.loadings.NOTE_DETAIL,
            isDetailModalOpened: reduxState.notes.isDetailModalOpened,
            noteData: reduxState.notes.noteData,
        }),
        {detailNoteModalOpen}
    ),
    translate
)(_DetailNoteModal)

export default DetailNoteModal
