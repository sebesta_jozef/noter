import {call, put, takeLatest} from 'redux-saga/effects'
import {
  HEALTH_CHECK_REQUEST,
  LOGIN_REQUEST,
} from './constants'
import {
  loginSuccess,
  loginError, healthCheckSuccess, healthCheckError,
} from './actions'
import auth from "../../lib/auth";
import libHttp from "../../lib/http";
import api from '../../lib/api'


export function* doLoginRequest(action) {
  try {
    const response = yield call([libHttp, 'post'], api.authenticate, {"username": action.username, "password": action.password})
    if (response.status === 200) {
      yield call(auth.authenticate, response.data.token)
      yield put(loginSuccess(response.data.token))
      window.location.href = "/"
    }
  } catch (error) {
    const e = error.response.data.error
    yield put(loginError(e.code, e.message))
  }
}

export function* watchLoginRequest() {
  yield takeLatest(LOGIN_REQUEST, doLoginRequest)
}

export function* doHealthCheckRequest() {
  try {
    const response = yield call([libHttp, 'get'], api.healthCheck)

    if (response.status === 200) {
      yield put(healthCheckSuccess())
    }
  } catch (error) {
    yield put(healthCheckError())
  }
}

export function* watchHealthCheckRequest() {
  yield takeLatest(HEALTH_CHECK_REQUEST, doHealthCheckRequest)
}