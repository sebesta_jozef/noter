import React, {FunctionComponent} from "react";
import {translate} from 'react-switch-lang'
import {Button, CircularProgress, DialogActions, DialogContent, FormControl} from "@material-ui/core";
import {Field} from "formik";
import {TextField} from "formik-material-ui";
import {compose} from 'recompose'
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        dialogActions: {
            marginTop: theme.spacing(3),
            display: "flex",
            justifyContent: "left",
            paddingLeft: "24px"
        },
    })
);

interface Props {
    t: ((arg0: string) => string),
    onClose: (() => void),
    loading: boolean,
}

const _NoteFormFields: FunctionComponent<Props> = props => {
    const {t, loading, onClose} = props
    const classes = useStyles();

    return (
        <React.Fragment>
            <DialogContent>
                <FormControl required fullWidth>
                    <Field
                        margin="normal"
                        fullWidth
                        type="text"
                        name="title"
                        component={TextField}
                        label={t('Form.title')}
                    />
                </FormControl>
                <FormControl required fullWidth>
                    <Field
                        margin="normal"
                        fullWidth
                        type="text"
                        name="description"
                        component={TextField}
                        label={t('Form.description')}
                    />
                </FormControl>
            </DialogContent>
            <DialogActions className={classes.dialogActions}>
                <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    size="large"
                    disabled={!!loading}
                >
                    {loading && <CircularProgress size={14} />}
                    {!loading && t('Button.saveNote')}
                </Button>
                <Button
                    autoFocus
                    onClick={() => onClose()}
                    variant={'outlined'}
                    color="primary"
                    size="large"
                >
                    {t('Button.cancel')}
                </Button>
            </DialogActions>
        </React.Fragment>
    )
}

const NoteFormFields = compose(
    translate
)(_NoteFormFields)

export default NoteFormFields