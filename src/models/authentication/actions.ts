import {
  HEALTH_CHECK_ERROR,
  HEALTH_CHECK_REQUEST,
  HEALTH_CHECK_SUCCESS,
  LOGIN_ERROR,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
} from './constants'

import {AuthenticationActionTypes} from "./actions.interfaces";

export const loginRequest = (username, password):AuthenticationActionTypes => ({
  type: LOGIN_REQUEST,
  username,
  password
})

export const loginError = (errorCode?, errorMessage?):AuthenticationActionTypes => ({
  type: LOGIN_ERROR,
  errorCode,
  errorMessage
})

export const loginSuccess = (token):AuthenticationActionTypes => ({
  type: LOGIN_SUCCESS,
  token,
})

export const healthCheckRequest = ():AuthenticationActionTypes => ({
  type: HEALTH_CHECK_REQUEST,
})

export const healthCheckSuccess = ():AuthenticationActionTypes => ({
  type: HEALTH_CHECK_SUCCESS,
})

export const healthCheckError = ():AuthenticationActionTypes => ({
  type: HEALTH_CHECK_ERROR,
})
