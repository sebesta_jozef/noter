import {HEALTH_CHECK_ERROR, HEALTH_CHECK_SUCCESS, LOGIN_ERROR, LOGIN_REQUEST, LOGIN_SUCCESS} from './constants'

import {AuthenticationActionTypes} from "./actions.interfaces";


export type State = {
  readonly token: string;
  readonly isServerUp: boolean;
  readonly username: string;
  readonly password: string;
  readonly errorCode?: string;
  readonly errorMessage?: string;
};


const initialState: State = {
  token: "",
  username: "",
  password: "",
  errorCode: "",
  errorMessage: "",
  isServerUp: false
}


const reducer = (state = initialState, action: AuthenticationActionTypes) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        token: action.token,
        errorCode: "",
        errorMessage: "",
      }

    case LOGIN_REQUEST:
      return {
        ...state,
        username: action.username,
        password: action.password,
      }
    case LOGIN_ERROR:
      return {
        ...state,
        errorCode: action.errorCode,
        errorMessage: action.errorMessage,
      }
    case HEALTH_CHECK_SUCCESS:
      return {
        ...state,
        isServerUp: true,
      }
    case HEALTH_CHECK_ERROR:
      return {
        ...state,
        isServerUp: false,
      }
    default:
      return state
  }
}

export default reducer
