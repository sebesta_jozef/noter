import {call, put, takeLatest} from 'redux-saga/effects'

import {
  NOTE_UPDATE_REQUEST,
  NOTES_LIST_REQUEST,
  NOTE_DETAIL_REQUEST,
  NOTE_DELETE_REQUEST,
  NOTE_CREATE_REQUEST,
} from './constants'
import {
  notesListSuccess,
  noteDetailError,
  notesListRequest,
  noteDetailSuccess,
  notesListError,
  noteUpdateError,
  noteUpdateSuccess,
  noteDeleteSuccess,
  noteDeleteError,
  noteCreateSuccess,
  noteCreateError,
  createNoteModalOpen
} from './actions'
import api from '../../lib/api'
import libHttp from "../../lib/http";

export function* doNotesListRequest() {
  try {
    const response = yield call([libHttp, 'get'], api.notes)
    if (response.status === 200) {
      yield put(notesListSuccess(response.data))
    }
  } catch (error) {
    yield put(notesListError())
  }
}

export function* watchNotesListRequest() {
  yield takeLatest(NOTES_LIST_REQUEST, doNotesListRequest)
}

export function* doNoteDetailRequest(action) {
  try {
    const response = yield call([libHttp, 'get'], `${api.notes}/${action.noteId}`)
    if (response.status === 200) {
      yield put(noteDetailSuccess(response.data))
    }
  } catch (error) {
    yield put(noteDetailError())
  }
}

export function* watchNoteDetailRequest() {
  yield takeLatest(NOTE_DETAIL_REQUEST, doNoteDetailRequest)
}

export function* doNoteUpdateRequest(action) {
  try {
    const response = yield call([libHttp, 'put'], `${api.notes}/${action.noteData.id}`, {
      ...action.noteData,
    })
    if (response.status === 200) {
      yield put(noteUpdateSuccess())
      yield put(notesListRequest())
    }
  } catch (error) {
    yield put(noteUpdateError())
  }
}

export function* watchNoteUpdateRequest() {
  yield takeLatest(NOTE_UPDATE_REQUEST, doNoteUpdateRequest)
}

export function* doNoteDeleteRequest(action) {
  try {
    const response = yield call([libHttp, 'delete'], `${api.notes}/${action.noteId}`)
    if (response.status === 200) {
      yield put(noteDeleteSuccess())
      yield put(notesListRequest())
    }
  } catch (error) {
    yield put(noteDeleteError())
  }
}

export function* watchNoteDeleteRequest() {
  yield takeLatest(NOTE_DELETE_REQUEST, doNoteDeleteRequest)
}

export function* doNoteCreateRequest(action) {
  try {
    const response = yield call([libHttp, 'post'], api.notes, {
      title: action.title,
      description: action.description,
    })
    if (response.status === 200) {
      yield put(noteCreateSuccess())
      yield put(notesListRequest())
      yield put(createNoteModalOpen(false))
    }
  } catch (error) {
    yield put(noteCreateError())
  }
}

export function* watchNoteCreateRequest() {
  yield takeLatest(NOTE_CREATE_REQUEST, doNoteCreateRequest)
}
