const auth = {
    isAuthenticated: (): boolean => Boolean(localStorage.getItem("token")),

    authenticate: (token: string) => {
        localStorage.setItem("token", token)
    },
    deauthenticate: ():void => {
        localStorage.removeItem("token")
        window.location.href = '/login';
    },
    token: (): null|string => {
        return localStorage.getItem("token")
    }
}

export default auth;