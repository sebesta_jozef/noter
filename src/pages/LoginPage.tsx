import React, {FunctionComponent, useEffect} from "react";
import {connect} from "react-redux";
import {RootState} from "../rootReducer";
import {healthCheckRequest, loginRequest} from "../models/authentication/actions";
import {compose} from 'recompose'
import {translate} from 'react-switch-lang'
import {Field, Form, Formik, FormikHelpers} from "formik";
import {Button, CircularProgress, FormControl} from "@material-ui/core";
import {TextField} from "formik-material-ui";
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import * as yup from 'yup';
import Alert from "@material-ui/lab/Alert";
import BasicLayout from "../components/BasicLayout";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            display: 'flex',
            flexWrap: 'wrap',
            width: 400,
            margin: `${theme.spacing(0)} auto`
        },
        alert: {
            height:"45px"
        },
        loginBtn: {
            marginTop: theme.spacing(2),
            flexGrow: 1
        },
        header: {
            textAlign: 'center',
            background: '#ccccc',
        },
        card: {
            marginTop: theme.spacing(10)
        }
    })
);



interface Props {
    loginRequest: ((arg0: string, arg1: string) => void),
    healthCheckRequest: (() => void),
    t: ((arg0: string) => string),
    loading: boolean,
    errorCode: string,
}


interface ICredentials {
    username:string,
    password:string,
}

const _LoginPage: FunctionComponent<Props> = props => {
    const {t, loading, loginRequest, errorCode, healthCheckRequest} = props
    const classes = useStyles();

    useEffect(() => {
        healthCheckRequest()
    })

    const validationSchema = yup.object().shape({
        username: yup.string().required(t('Form.required')),
        password: yup.string().required(t('Form.required'))
    });

    const handleSubmit = (values : ICredentials, formikHelpers: FormikHelpers<any>) => {
        loginRequest(values.username, values.password)
        formikHelpers.setSubmitting(false);
    }

    return (
        <BasicLayout>
            <div className={classes.container}>
                <Card className={classes.card} raised={true}>
                    <Formik
                        initialValues={{username: "", password: ""}}
                        validationSchema={validationSchema}
                        onSubmit={handleSubmit}
                    >
                        {(props) => {
                            return (
                                <Form>
                                    <CardHeader className={classes.header} title={t("Login.title")} />
                                    <CardContent>
                                        <div className={classes.alert}>
                                        { errorCode === "BAD_CREDENTIALS" && (
                                            <Alert severity="error">{t("Login.badCredentials")}</Alert>
                                        )}
                                        </div>
                                        <FormControl required fullWidth>
                                            <Field
                                                fullWidth
                                                margin="normal"
                                                type="text"
                                                component={TextField}
                                                name="username"
                                                label={t('Form.username')}
                                            />
                                        </FormControl>
                                        <FormControl required fullWidth>
                                            <Field
                                                margin="normal"
                                                fullWidth
                                                type="text"
                                                component={TextField}
                                                name="password"
                                                label={t('Form.password')}
                                            />
                                        </FormControl>
                                    </CardContent>
                                    <CardActions>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            type="submit"
                                            size="large"
                                            disabled={!!loading}
                                            className={classes.loginBtn}
                                        >
                                            {loading && <CircularProgress size={14} />}
                                            {!loading && t('Button.login')}
                                        </Button>
                                    </CardActions>
                                </Form>
                            )
                        }}
                    </Formik>
                </Card>
            </div>
        </BasicLayout>
    )
}


const LoginPage = compose(
    connect(
        (reduxState: RootState) => ({
            loading: reduxState.loadings.LOGIN,
            errorCode: reduxState.authentication.errorCode,
            errorMessage: reduxState.authentication.errorMessage,
        }),
        {loginRequest, healthCheckRequest}
    ),
    translate
)(_LoginPage);

export default LoginPage;
