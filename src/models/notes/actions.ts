import {
  NOTES_LIST_REQUEST,
  NOTES_LIST_ERROR,
  NOTES_LIST_SUCCESS,
  NOTE_DETAIL_ERROR,
  NOTE_DETAIL_REQUEST,
  NOTE_DETAIL_SUCCESS,
  NOTE_UPDATE_ERROR,
  NOTE_UPDATE_REQUEST,
  NOTE_UPDATE_SUCCESS,
  NOTE_DELETE_ERROR,
  NOTE_DELETE_REQUEST,
  NOTE_DELETE_SUCCESS,
  NOTE_CREATE_REQUEST,
  NOTE_CREATE_ERROR,
  NOTE_CREATE_SUCCESS,
  CREATE_NOTE_MODAL_OPEN, UPDATE_NOTE_MODAL_OPEN, DETAIL_NOTE_MODAL_OPEN, RESET_FLAGS, DELETE_NOTE_MODAL_OPEN
} from './constants'

import {NoteActionTypes} from "./actions.interfaces";

export const notesListRequest = ():NoteActionTypes => ({
  type: NOTES_LIST_REQUEST,
})

export const notesListError = ():NoteActionTypes => ({
  type: NOTES_LIST_ERROR,
})

export const notesListSuccess = (notesData):NoteActionTypes => ({
  type: NOTES_LIST_SUCCESS,
  notesData,
})

export const noteDetailRequest = (noteId):NoteActionTypes => ({
  type: NOTE_DETAIL_REQUEST,
  noteId,
})

export const noteDetailError = ():NoteActionTypes => ({
  type: NOTE_DETAIL_ERROR,
})

export const noteDetailSuccess = (noteData):NoteActionTypes => ({
  type: NOTE_DETAIL_SUCCESS,
  noteData,
})

export const noteUpdateRequest = (noteData):NoteActionTypes => ({
  type: NOTE_UPDATE_REQUEST,
  noteData
})

export const noteUpdateError = ():NoteActionTypes => ({
  type: NOTE_UPDATE_ERROR,
})

export const noteUpdateSuccess = ():NoteActionTypes => ({
  type: NOTE_UPDATE_SUCCESS,
  isUpdated: true,
})

export const noteDeleteRequest = (noteId):NoteActionTypes => ({
  type: NOTE_DELETE_REQUEST,
  noteId,
})

export const noteDeleteError = ():NoteActionTypes => ({
  type: NOTE_DELETE_ERROR,
})

export const noteDeleteSuccess = ():NoteActionTypes => ({
  type: NOTE_DELETE_SUCCESS,
  isDeleted: true,
})

export const noteCreateRequest = (title, description):NoteActionTypes => ({
  type: NOTE_CREATE_REQUEST,
  title,
  description
})

export const noteCreateError = ():NoteActionTypes => ({
  type: NOTE_CREATE_ERROR,
})

export const noteCreateSuccess = ():NoteActionTypes => ({
  type: NOTE_CREATE_SUCCESS,
  isCreated: true,
})

export const resetFlags = ():NoteActionTypes => ({
  type: RESET_FLAGS,
  isUpdated: false,
  isCreated: false,
  isDeleted: false,
})

export const createNoteModalOpen = (isOpened):NoteActionTypes => ({
  type: CREATE_NOTE_MODAL_OPEN,
  isOpened,
})

export const updateNoteModalOpen = (isOpened, data):NoteActionTypes => ({
  type: UPDATE_NOTE_MODAL_OPEN,
  isOpened,
  noteData: data,
})

export const detailNoteModalOpen = (isOpened, data):NoteActionTypes => ({
  type: DETAIL_NOTE_MODAL_OPEN,
  isOpened,
  noteData: data,
})

export const deleteNoteModalOpen = (isOpened, noteId, noteTitle):NoteActionTypes => ({
  type: DELETE_NOTE_MODAL_OPEN,
  isOpened,
  noteId,
  noteTitle
})
