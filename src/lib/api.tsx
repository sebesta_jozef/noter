const baseUrl = 'http://localhost:8080/'

const api = {
    notes: `${baseUrl}notes`,
    authenticate: `${baseUrl}authenticate`,
    healthCheck: `${baseUrl}health-check`
}

export default api