import React, { FunctionComponent} from 'react'
import {
  Button,
  Tooltip,
  Box, Typography,
} from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'
import {connect} from 'react-redux'
import {translate} from 'react-switch-lang'
import {compose} from 'recompose'

import {
  updateNoteModalOpen, detailNoteModalOpen, deleteNoteModalOpen, noteDetailRequest
} from '../models/notes/actions'

import { NoteDataModel } from "../interfaces/NoteModel";
import { RootState } from "../rootReducer";

import TimeAgo from 'react-timeago'
import czechStrings from 'react-timeago/lib/language-strings/cs'
import englishStrings from 'react-timeago/lib/language-strings/en'
import buildFormatter from 'react-timeago/lib/formatters/buildFormatter'
import Cookies from "js-cookie";


interface Props extends NoteDataModel {
  updateNoteModalOpen: ((arg0: boolean, arg1: NoteDataModel) => void);
  detailNoteModalOpen: ((arg0: boolean, arg1: NoteDataModel) => void);
  deleteNoteModalOpen: ((arg0: boolean, arg1: string, arg2: string) => void);
  noteDetailRequest: ((arg0: string) => void);
  noteData: NoteDataModel,
  t: ((arg0: string) => string),
}

const _Note: FunctionComponent<Props> = props => {

  const {updateNoteModalOpen, detailNoteModalOpen, deleteNoteModalOpen, noteDetailRequest, noteData, t} = props
  const lang = Cookies.get('language') || 'en'
  const formatter = lang === "en" ? buildFormatter(englishStrings) : buildFormatter(czechStrings)
  return (
    <div>
        <Box
            display="flex"
            justify-content="space-between"
            p={1}
            m={1}
            bgcolor="background.paper"
            css={{maxWidth: 740}}
        >
          <Box width={'370px'} display={"flex"} flexDirection={"column"} justifyContent={"center"}>
            {noteData.title}
          </Box>
          <Box width={"130px"} display={"flex"} flexDirection={"column"} justifyContent={"center"}>
            <Typography variant="caption" display="block" gutterBottom style={{color:"#ccccc", fontStyle: "italic", fontSize: 10}}>
              <TimeAgo date={noteData.modifiedAt} formatter={formatter} />
            </Typography>
          </Box>
          <Box p={1}>
            <Tooltip title={t('Button.detail')}>
              <Button
                  variant="outlined"
                  color="primary"
                  onClick={() => {
                    detailNoteModalOpen(true, noteData)
                    noteDetailRequest(noteData.id)
                  }}
              >
                <SearchIcon />
              </Button>
            </Tooltip>
          </Box>
          <Box p={1}>
            <Tooltip title={t('Button.edit')}>
              <Button
                variant="outlined"
                color="primary"
                onClick={() => updateNoteModalOpen(true, noteData)}
              >
                <EditIcon />
              </Button>
            </Tooltip>
          </Box>
          <Box p={1}>
            <Tooltip title={t('Button.delete')}>
              <Button
                  variant="contained"
                  color="secondary"
                  onClick={() => deleteNoteModalOpen(true, noteData.id, noteData.title)}
              >
                <DeleteIcon />
              </Button>
            </Tooltip>
          </Box>
        </Box>
    </div>
  )
}


const Note = compose(
  connect(
    (reduxState: RootState) => ({
    }),
    {updateNoteModalOpen, detailNoteModalOpen, deleteNoteModalOpen, noteDetailRequest}
  ),
  translate
)(_Note)

export default Note
