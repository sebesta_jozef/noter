import {
  LOGIN_REQUEST,
  LOGIN_ERROR,
  LOGIN_SUCCESS, HEALTH_CHECK_REQUEST, HEALTH_CHECK_SUCCESS, HEALTH_CHECK_ERROR,
} from './constants'


interface LoginRequestAction {
  type: typeof LOGIN_REQUEST,
  username: string,
  password: string,
}

interface LoginSuccessAction {
  type: typeof LOGIN_SUCCESS,
  token: string,
}

interface LoginErrorAction {
  type: typeof LOGIN_ERROR,
  errorCode?: string,
  errorMessage?: string,
}

interface HealthCheckRequest {
  type: typeof HEALTH_CHECK_REQUEST,
}

interface HealthCheckSuccess {
  type: typeof HEALTH_CHECK_SUCCESS,
}

interface HealthCheckError {
  type: typeof HEALTH_CHECK_ERROR,
}

export type AuthenticationActionTypes = LoginRequestAction | LoginErrorAction | LoginSuccessAction | HealthCheckRequest | HealthCheckSuccess | HealthCheckError
